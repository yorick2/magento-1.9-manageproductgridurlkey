adds a url key column into the manage products grid which links to the relevant products page on the front end
![alt tag](https://raw.githubusercontent.com/yorick2/-magento-1.9-ManageProductGridUrlKey/screenshot.png)
